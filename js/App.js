const music=new Audio("./audio/1.mp3")


const masterPlay=document.getElementById("masterPlay")
const wave= document.querySelector(".wave")
console.log(masterPlay)
masterPlay.addEventListener("click",()=>{
    if(music.paused||music.currentTime<=0){
        console.log("Asdsda")
        music.play()
        masterPlay.classList.remove("bi-play-fill")
        masterPlay.classList.add("bi-pause-fill")
        wave.classList.add("active2")
        
    }else{
        music.pause()
        masterPlay.classList.add("bi-play-fill")
        masterPlay.classList.remove("bi-pause-fill")
        wave.classList.remove("active2")
    }
})


const makeAllPlays=()=>{
    Array.from(document.getElementsByClassName("playListPlay")).map((element,i)=>{
        element.classList.add("bi-play-circle-fill")
        element.classList.remove("bi-pause-circle-fill")
        })
}
const makeAllBackground=()=>{
    Array.from(document.getElementsByClassName("songItem")).map((element,i)=>{
        element.style.background="rgb(105,105,170,0)"
        })
}


let index=0
const poster_master_play=document.getElementById("poster_master_play")
const title=document.getElementById("title")
Array.from(document.getElementsByClassName("playListPlay")).map((element,i)=>{
    element.addEventListener("click",(e)=>{
        index=e.target.id
        makeAllPlays()
        e.target.classList.remove("bi-play-circle-fill")
        e.target.classList.add("bi-pause-circle-fill")
        music.src=`audio/${index}.mp3`
        poster_master_play.src=`img/${index}.jpg`
        music.play()
        let song_title=songs.filter(ele=>ele.id==index)
        song_title.map(ele=>{
            title.innerHTML=ele.songName
        })
        masterPlay.classList.remove("bi-play-fill")
        masterPlay.classList.add("bi-pause-fill")
        wave.classList.add("active2")
        // music.addEventListener("end",()=>{
        //     masterPlay.classList.add("bi-play-fill")
        //     masterPlay.classList.remove("bi-pause-fill")
        //     wave.classList.remove("active2")
           
        // })
        
        makeAllBackground()
        Array.from(document.getElementsByClassName("songItem"))[`${i}`].style.background="rgb(105,105,170,0.1)"
    })
})

const currentStart=document.getElementById("currentStart")
const currentEnd=document.getElementById("currentEnd")
const seek=document.getElementById("seek")
const bar2=document.getElementById("bar2")
const dot=document.getElementsByClassName("dot")[0]

music.addEventListener("timeupdate",()=>{
    let music_curr=music.currentTime
    let music_dur=music.duration
    let min=Math.floor(music_dur/60)
    let sec=Math.floor(music_dur%60)
    if(sec<10){
        sec=`0 ${sec}`
    }
    currentEnd.innerHTML=`${min}:${sec}`

    let min1=Math.floor(music_curr/60)
    let sec1=Math.floor(music_curr%60)
    if(sec1<10){
        sec1=`0${sec1}`
    }
    currentStart.innerHTML=`${min1}:${sec1}`

    let progressBar=parseInt((music.currentTime/music.duration)*100)
    seek.value=progressBar
    let seekbar=seek.value
    bar2.style.width=`${seekbar}%`
    dot.style.left=`${seekbar}%`
})

seek.addEventListener("change",()=>{
    music.currentTime=seek.value*music.duration/100
})
music.addEventListener("end",()=>{
    masterPlay.classList.add("bi-pause-fill")
    wave.classList.add("active2")
    index++;
    music.src=`audio/${index}.mp3`
    poster_master_play.src=`img/${index}.jpg`
    music.play()
    let song_title=songs.filter(ele=>ele.id==index)
    song_title.map(ele=>{
        title.innerHTML=ele.songName
    })
    makeAllBackground()
    Array.from(document.getElementsByClassName("songItem"))[`${index-1}`].style.background="rgb(105,105,170,0.1)"
})

const vol_icon=document.getElementById("vol_icon")
const vol=document.getElementById("vol")
const vol_dot=document.getElementById("vol_dot")
const vol_bar=document.getElementsByClassName("vol_bar")[0]
console.log(vol_icon)
vol.addEventListener("change",()=>{
    if(vol.value==0){
        vol_icon.classList.remove("bi-volume-down-fill");
        vol_icon.classList.add("bi-volume-mute-fill");
        vol_icon.classList.remove("bi-volume-up-fill");
    }
    if(vol.value>0){
        vol_icon.classList.add("bi-remove-down-fill");
        vol_icon.classList.remove("bi-remove-mute-fill");
        vol_icon.classList.remove("bi-remove-up-fill");
    }
    if(vol.value>50){
        vol_icon.classList.remove("bi-volume-down-fill");
        vol_icon.classList.remove("bi-volume-mute-fill");
        vol_icon.classList.add("bi-volume-up-fill");
    }
    let vol_a=vol.value;
    vol_bar.style.width=`${vol_a}%`
    vol_dot.style.left=`${vol_a}%`
    music.volume=vol_a/100
})

const back=document.getElementById("back")

const next=document.getElementById("next")
back.addEventListener("click",()=>{

    index-=1;
    if(index<1){
        index=Array.from(document.getElementsByClassName("songItem")).length
    }
    music.src=`audio/${index}.mp3`
    poster_master_play.src=`img/${index}.jpg`
    music.play()
    let song_title=songs.filter(ele=>ele.id==index)
    song_title.map(ele=>{
        title.innerHTML=ele.songName
    })
    makeAllPlays()
    document.getElementById(`${index}`).classList.remove("bi-play-fill")
    document.getElementById(`${index}`).classList.add("bi-pause-fill")
    makeAllBackground()
    Array.from(document.getElementsByClassName("songItem"))[`${index-1}`].style.background="rgb(105,105,170,0.1)"
})

next.addEventListener("click",()=>{
    index-=0
    index+=1;
    if(index>Array.from(document.getElementsByClassName("songItem")).length){
        index=1
    }

    music.src=`audio/${index}.mp3`
    poster_master_play.src=`img/${index}.jpg`
    music.play()
    let song_title=songs.filter(ele=>ele.id==index)
    song_title.map(ele=>{
        title.innerHTML=ele.songName
    })
    makeAllPlays()
    document.getElementById(`${index}`).classList.remove("bi-play-fill")
    document.getElementById(`${index}`).classList.add("bi-pause-fill")
    makeAllBackground()
    Array.from(document.getElementsByClassName("songItem"))[`${index-1}`].style.background="rgb(105,105,170,0.1)"
})

const left_scroll=document.getElementById("left_scroll")
const right_scroll=document.getElementById("right_scroll")
const pop_song=document.getElementsByClassName("pop_song")[0]
left_scroll.addEventListener("click",()=>{
    pop_song.scrollLeft-=330
})
right_scroll.addEventListener("click",()=>{
    pop_song.scrollLeft+=330
})

const left_scrolls=document.getElementById("left_scrolls")
const right_scrolls=document.getElementById("right_scrolls")
const item=document.getElementsByClassName("item")[0]
left_scrolls.addEventListener("click",()=>{
    item.scrollLeft-=330
})
right_scrolls.addEventListener("click",()=>{
    item.scrollLeft+=330
})


const songItem=document.querySelectorAll(".songItem")

for(let i=0;i<songItem.length;i++){
    songItem[i].getElementsByTagName("img")[0].src=songs[i].poster
    songItem[i].getElementsByTagName("h5")[0].innerHTML=songs[i].songName
}